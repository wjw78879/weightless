﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickaxe : MonoBehaviour
{
    [SerializeField] private float m_attackMaxCoolDown = 1.2f;

    private Animator m_animator;

    private float m_coolDown = 0f;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
    }

    private void Update()
    {
        m_coolDown = Mathf.Clamp(m_coolDown - Time.deltaTime, 0f, m_attackMaxCoolDown);
        if (m_coolDown < Mathf.Epsilon && InputManager.GetAttack())
        {
            m_coolDown = m_attackMaxCoolDown;
            m_animator.SetTrigger("Attack");
        }
    }
}
