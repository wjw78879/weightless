﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootItemDetector : MonoBehaviour
{
    [SerializeField] private float m_detectDistance = 2f;
    [SerializeField] private LayerMask m_lootMask;

    private Camera m_mainCamera;

    private RaycastHit m_hitInfo;
    private LootItem m_currentSelect;
    private LootItem m_detected;

    private void Start()
    {
        m_mainCamera = Camera.main;
        m_currentSelect = null;
    }

    private void Update()
    {
        if (m_currentSelect && InputManager.GetPickUp())
        {
            FindObjectOfType<HandManager>().ChangeEquipment(ItemManager.GetEquipmentOnHand(m_currentSelect.GetItemIndex()));
            Destroy(m_currentSelect.gameObject);
            m_currentSelect = null;
        }
    }

    private void FixedUpdate()
    {
        if (Physics.Raycast(m_mainCamera.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f, 0f)), out m_hitInfo, m_detectDistance, m_lootMask))
        {
            m_detected = m_hitInfo.collider.GetComponent<LootItem>();
            
        } else
        {
            m_detected = null;
        }
        if (m_detected)
        {
            if (m_detected != m_currentSelect)
            {
                if (m_currentSelect)
                {
                    m_currentSelect.SetOutline(false);
                }
                m_detected.SetOutline(true);
            }
        } else
        {
            if (m_currentSelect)
            {
                m_currentSelect.SetOutline(false);
            }
        }
        m_currentSelect = m_detected;
    }
}
