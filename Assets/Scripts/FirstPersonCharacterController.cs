﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MoveState
{
    Idling,
    Running,
    Dashing
}

public class FirstPersonCharacterController : MonoBehaviour
{
    public Transform containerTransform;
    public Transform groundCheckTransform;

    [SerializeField] private float m_minXRotation = -89f;
    [SerializeField] private float m_maxXRotation = 89f;
    [SerializeField] private float m_horizontalSensitivity = 20f;
    [SerializeField] private float m_verticalSensitivity = 10f;

    [SerializeField] private float m_defaultMoveStartSmoothTime = 0.1f;
    [SerializeField] private float m_defaultMoveStopSmoothTime = 0.05f;
    [SerializeField] private float m_defaultAirSmoothMultiplier = 3f;

    [SerializeField] private float m_groundCheckRadius = 0.52f;
    [SerializeField] private LayerMask m_groundMask;

    [SerializeField] private float m_defaultGravity = -9.81f;
    [SerializeField] private float m_defaultWalkSpeed = 5f;

    private CharacterController m_characterController;
    private Animator m_animator;

    private bool m_isOnControl;
    private bool m_isGrounded;
    private MoveState m_moveState;

    private Vector3 m_viewInput;
    private float m_xRotation;
    private Vector3 m_rawInput;
    private float m_rawMagnitude = 0f;
    private Vector3 m_smoothedInput;
    private float m_smoothedMagnitude = 0f;
    private Vector3 m_velocity;

    private float m_verticalSpeed = 0f;
    private Vector3 m_totalMovement;

    private void Start()
    {
        m_characterController = GetComponent<CharacterController>();
        m_animator = GetComponent<Animator>();
        m_xRotation = containerTransform.rotation.eulerAngles.x;
        m_isOnControl = true;
        m_moveState = MoveState.Idling;

        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        if (m_isOnControl)
        {
            m_viewInput = InputManager.GetViewInput();
            m_rawInput = InputManager.GetRawInput();
        } else
        {
            m_viewInput = Vector3.zero;
            m_rawInput = Vector3.zero;
        }

    }

    private void FixedUpdate()
    {
        //Ground check and apply gravity
        m_isGrounded = Physics.CheckSphere(groundCheckTransform.position, m_groundCheckRadius, m_groundMask);
        if (m_isGrounded && m_verticalSpeed < 0f)
        {
            m_verticalSpeed = -2f;
        }
        else
        {
            m_verticalSpeed += m_defaultGravity * Time.deltaTime;
        }

        //Calculate move && change state
        m_rawMagnitude = m_rawInput.magnitude;
        if (m_rawMagnitude > 0.1f)
        {
            m_moveState = MoveState.Running;
            m_rawInput.Normalize();
            m_smoothedInput = Vector3.SmoothDamp(m_smoothedInput, m_rawInput, ref m_velocity, m_defaultMoveStartSmoothTime * (m_isGrounded ? m_defaultAirSmoothMultiplier : 1f));
        } else
        {
            m_moveState = MoveState.Idling;
            m_smoothedInput = Vector3.SmoothDamp(m_smoothedInput, Vector3.zero, ref m_velocity, m_defaultMoveStopSmoothTime * (m_isGrounded ? m_defaultAirSmoothMultiplier : 1f));
        }
        m_smoothedMagnitude = m_smoothedInput.magnitude;

        //Calculate X rotation
        m_xRotation += m_viewInput.y * m_verticalSensitivity;
        m_xRotation = Mathf.Clamp(m_xRotation, m_minXRotation, m_maxXRotation);
        
        //Apply X and Y rotation
        transform.Rotate(m_horizontalSensitivity * m_viewInput.x * Vector3.up);
        containerTransform.localRotation = Quaternion.Euler(0f, 0f, m_xRotation);

        //Apply movement
        m_totalMovement = transform.TransformDirection(m_smoothedInput) * m_defaultWalkSpeed;
        m_totalMovement.y = m_verticalSpeed;
        m_characterController.Move(m_totalMovement * Time.deltaTime);

        //Update animator
        m_animator.SetInteger("MoveState", (int)m_moveState);
        if (m_moveState == MoveState.Running)
        {
            m_animator.SetFloat("Horizontal", m_rawInput.x);
            m_animator.SetFloat("Vertical", m_rawInput.z);
        }
    }
}
