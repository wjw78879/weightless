﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKTargetPlacer : MonoBehaviour
{
    public Transform targetTransform;

    private void Start()
    {
        transform.position = targetTransform.position;
        transform.rotation = targetTransform.rotation;
    }

    private void Update()
    {
        transform.position = targetTransform.position;
        transform.rotation = targetTransform.rotation;
    }
}
