﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandManager : MonoBehaviour
{
    public Animator fingerAnimator;

    public Transform leftHandIKTarget;
    public Transform rightHandIKTarget;
    public Transform defaultLeftHandIK;
    public Transform defaultRightHandIK;

    private GameObject currentEquipment;
    private Transform leftHandIK;
    private Transform rightHandIK;

    private void Start()
    {
        fingerAnimator.SetInteger("EquipmentIndex", -1);
    }

    private void Update()
    {
        if (leftHandIK)
        {
            leftHandIKTarget.position = leftHandIK.position;
            leftHandIKTarget.rotation = leftHandIK.rotation;

        } else
        {
            leftHandIKTarget.position = defaultLeftHandIK.position;
            leftHandIKTarget.rotation = defaultLeftHandIK.rotation;
        }
        if (rightHandIK)
        {
            rightHandIKTarget.position = rightHandIK.position;
            rightHandIKTarget.rotation = rightHandIK.rotation;
        } else
        {
            rightHandIKTarget.position = defaultRightHandIK.position;
            rightHandIKTarget.rotation = defaultRightHandIK.rotation;
        }
    }

    public void ChangeEquipment(EquipmentOnHandInfo equipmentInfo)
    {
        if (currentEquipment)
        {
            Destroy(currentEquipment);
        }
        currentEquipment = Instantiate(equipmentInfo.handObjectPrefab, transform);
        Equipment equipment = currentEquipment.GetComponent<Equipment>();
        if (equipment)
        {
            leftHandIK = equipment.GetLeftHandIK();
            rightHandIK = equipment.GetRightHandIK();
            fingerAnimator.SetInteger("EquipmentIndex", equipmentInfo.itemIndex);
        } else
        {
            Debug.LogError("Component <Equipment> not found on item index: " + equipmentInfo.itemIndex);
        }
    }
}
