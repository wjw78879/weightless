﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    public string localizationKey;

    private Text m_text;

    private void Start()
    {
        m_text = GetComponent<Text>();
        UpdateText();
    }

    public void UpdateText()
    {
        if (m_text)
        {
            m_text.text = LocalizationManager.GetLocalizedString(localizationKey);
        }
    }
}
