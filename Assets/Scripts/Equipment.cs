﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : MonoBehaviour
{
    public Transform leftHandIK;
    public Transform rightHandIK;

    private Animator m_animator;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
    }

    public Transform GetLeftHandIK()
    {
        return leftHandIK;
    }

    public Transform GetRightHandIK()
    {
        return rightHandIK;
    }
}
