﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    private static ItemManager m_instance = null;

    [SerializeField] private ItemInfo[] m_items;
    [SerializeField] private EquipmentOnHandInfo[] m_equipmentsOnHand;
    [SerializeField] private EquipmentOnBackInfo[] m_equipmentsOnBack;
    [SerializeField] private EquipmentOnFeetInfo[] m_equipmentsOnFeet;

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
        } else if (m_instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public static ItemInfo GetItemInfo(int index)
    {
        if (index < m_instance.m_items.Length)
        {
            return m_instance.m_items[index];
        } else
        {
            Debug.LogError("Item index requested out of range: " + index);
            return null;
        }
    }

    public static EquipmentOnHandInfo GetEquipmentOnHand(int index)
    {
        foreach (EquipmentOnHandInfo equipment in m_instance.m_equipmentsOnHand)
        {
            if (equipment.itemIndex == index)
            {
                return equipment;
            }
        }
        return null;
    }

    public static EquipmentOnBackInfo GetEquipmentOnBack(int index)
    {
        foreach (EquipmentOnBackInfo equipment in m_instance.m_equipmentsOnBack)
        {
            if (equipment.itemIndex == index)
            {
                return equipment;
            }
        }
        return null;
    }

    public static EquipmentOnFeetInfo GetEquipmentOnFeet(int index)
    {
        foreach (EquipmentOnFeetInfo equipment in m_instance.m_equipmentsOnFeet)
        {
            if (equipment.itemIndex == index)
            {
                return equipment;
            }
        }
        return null;
    }
}

[System.Serializable]
public class ItemInfo
{
    public string nameLocalizationKey;
    public string descriptionLocalizationKey;
    public Sprite icon;
    public GameObject lootPrefab;
}

[System.Serializable]
public class EquipmentOnHandInfo
{
    public int itemIndex;
    public GameObject handObjectPrefab;
}

[System.Serializable]
public class EquipmentOnBackInfo
{
    public int itemIndex;
    public GameObject backObjectPrefab;
}

[System.Serializable]
public class EquipmentOnFeetInfo
{
    public int itemIndex;
    public GameObject feetObjectPrefab;
}