﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public enum Language
{
    English,
    Chinese
}

public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager m_instance = null;

    [SerializeField] private Language m_defaultLanguage = Language.English;

    private Dictionary<string, TextAsset> m_localizationFiles = new Dictionary<string, TextAsset>();
    private Dictionary<string, string> m_localizationText = new Dictionary<string, string>();

    private Language m_language;

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
        }
        else if (m_instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        m_language = m_defaultLanguage;
        SetupLocalizationFiles();
        SetupLocalization();
    }

    void SetupLocalizationFiles()
    {
        foreach (Language language in Language.GetValues(typeof(Language)))
        {
            string textAssetPath = "Localization/" + language.ToString();
            TextAsset textAsset = (TextAsset)Resources.Load(textAssetPath);
            if (textAsset)
            {
                m_localizationFiles[textAsset.name] = textAsset;
                //Debug.Log("Text Asses: " + textAsset.name);
            }
            else
            {
                Debug.LogError("TextAssetPath not found: " + textAssetPath);
            }
        }
    }

    void SetupLocalization()
    {
        TextAsset textAsset;
        if (m_localizationFiles.ContainsKey(m_language.ToString()))
        {
            //Debug.Log("Selected language: " + m_language);
            textAsset = m_localizationFiles[m_language.ToString()];
        }
        else
        {
            Debug.LogError("Couldn't find localization file for: " + m_language);
            textAsset = m_localizationFiles[Language.English.ToString()];
        }

        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.LoadXml(textAsset.text);

        XmlNodeList entryList = xmlDocument.GetElementsByTagName("Entry");

        foreach (XmlNode entry in entryList)
        {
            if (!m_localizationText.ContainsKey(entry.FirstChild.InnerText))
            {
                //Debug.Log("Added key: " + entry.FirstChild.InnerText + " with value: " + entry.LastChild.InnerText);
                m_localizationText.Add(entry.FirstChild.InnerText, entry.LastChild.InnerText);
            }
            else
            {
                Debug.LogError("Duplicate Localization key detected: " + entry.FirstChild.InnerText);
            }
        }
    }

    public static void SwitchLanguage(Language language)
    {
        m_instance.m_language = language;
        m_instance.m_localizationText.Clear();
        m_instance.SetupLocalization();
        foreach (LocalizedText text in FindObjectsOfType<LocalizedText>())
        {
            text.UpdateText();
        }
    }

    public static string GetLocalizedString(string key)
    {
        string localizedString = "";
        if (!m_instance.m_localizationText.TryGetValue(key, out localizedString))
        {
            localizedString = "LOC KEY: " + key;
        }

        return localizedString;
    }
}
