﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private static InputManager m_instance = null;

    private Vector3 m_viewInput = Vector3.zero;
    private Vector3 m_rawInput = Vector3.zero;

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
        } else if (m_instance != this)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(gameObject);
    }

    public static Vector3 GetViewInput()
    {
        m_instance.m_viewInput.x = Input.GetAxisRaw("Mouse X") + Input.GetAxisRaw("Joystick X");
        m_instance.m_viewInput.y = Input.GetAxisRaw("Mouse Y") + Input.GetAxisRaw("Joystick Y");
        return m_instance.m_viewInput;
    }

    public static Vector3 GetRawInput()
    {
        m_instance.m_rawInput.x = Input.GetAxisRaw("Horizontal");
        m_instance.m_rawInput.z = Input.GetAxisRaw("Vertical");
        if (m_instance.m_rawInput.magnitude > 1f)
        {
            m_instance.m_rawInput.Normalize();
        }
        return m_instance.m_rawInput;
    }

    public static bool GetPickUp()
    {
        return Input.GetButtonDown("PickUp");
    }

    public static bool GetAttack()
    {
        return Input.GetButtonDown("Fire1");
    }
}
