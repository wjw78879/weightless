﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootItem : MonoBehaviour
{
    public Outline outline;

    [SerializeField] private int m_itemIndex;

    private void Start()
    {
        outline.enabled = false;
    }

    public int GetItemIndex()
    {
        return m_itemIndex;
    }

    public void SetOutline(bool tf)
    {
        outline.enabled = tf;
    }
}
